package com.example.ingresospora

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.util.Patterns
import android.view.View
import android.widget.Toast
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    lateinit var ref: DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun acceder(view: View) {
        val pattern = Patterns.EMAIL_ADDRESS
        if(pattern.matcher(edtUser.text.toString()).matches() || (edtUser.text.toString().isNullOrEmpty())){
            val mail:String=edtUser.text.toString()

            ref = FirebaseDatabase.getInstance().reference
            val mailRef = ref.child("Users").orderByChild("mail").equalTo(mail)

            val valueEventListener = object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    if (dataSnapshot.children.count() > 0){
                        existeUsuario(true)
                    }else{
                        existeUsuario(false)
                    }
                }

                override fun onCancelled(p0: DatabaseError) {
                    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                }

            }
            mailRef.addListenerForSingleValueEvent(valueEventListener)
        }else{
            Toast.makeText(this, "Error en la estructura del correo", Toast.LENGTH_SHORT).show()
        }


    }

    fun existeUsuario(existe: Boolean){
        if(existe){
            val intent = Intent(this, UsersList::class.java)
            startActivity(intent )
        }else{
            val intent = Intent(this, Registro::class.java)
            startActivity(intent )
        }
    }

}
