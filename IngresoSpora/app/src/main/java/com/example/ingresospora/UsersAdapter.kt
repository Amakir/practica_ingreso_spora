package com.example.ingresospora

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView

class UsersAdapter(val mCtx: Context, val layoutResId: Int, val usersList: List<Users>)
    : ArrayAdapter<Users>(mCtx, layoutResId, usersList){


    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val layoutInflater : LayoutInflater = LayoutInflater.from(mCtx)
        val view: View = layoutInflater.inflate(layoutResId, null)
        val textViewUserId = view.findViewById<TextView>(R.id.txtUserId)

        val user = usersList[position]

        textViewUserId.text = user.mail

        return view
    }

}