package com.example.ingresospora

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ListView
import android.widget.Toast
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_users_list.*

class UsersList : AppCompatActivity() {

    lateinit var listView: ListView
    lateinit var currentUser: String

    lateinit var ref: DatabaseReference
    lateinit var userList: MutableList<Users>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_users_list)

        listView = findViewById(R.id.listUsers)

        userList = mutableListOf()
        ref = FirebaseDatabase.getInstance().getReference("Users")

        ref.addValueEventListener(object: ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
            }

            override fun onDataChange(p0: DataSnapshot) {
                if(p0!!.exists()){
                    userList.clear()
                    for(h in p0.children){
                        val user = h.getValue(Users::class.java)
                        userList.add(user!!)
                    }
                    val adapter = UsersAdapter(applicationContext, R.layout.users, userList)
                    listView.adapter = adapter
                }
            }

        })


        listView.onItemClickListener = AdapterView.OnItemClickListener{ parent, view, position, id ->
            val item = parent.getItemAtPosition(position) as Users
            edtMailList.setText(item.mail)
            edtPhoneList.setText(item.phone)
            edtAddressList.setText(item.address)
            edtDateList.setText(item.date)
            currentUser = item.userId
        }




    }

    fun eliminar(view: View) {
        ref = FirebaseDatabase.getInstance().getReference("Users")
            .child(currentUser)
        ref.removeValue()
        edtMailList.setText("")
        edtPhoneList.setText("")
        edtAddressList.setText("")
        edtPhoneList.setText("")
        edtDateList.setText("")
    }

    fun addUser(view: View) {
        val intent = Intent(this, Registro::class.java)
        startActivity(intent )
    }
}
