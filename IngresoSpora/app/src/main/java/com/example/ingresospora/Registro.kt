package com.example.ingresospora

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.ContentResolver
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.webkit.MimeTypeMap
import android.widget.Toast
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_registro.*
import java.util.regex.Pattern
import android.util.Patterns
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import androidx.core.app.ComponentActivity
import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import java.text.ParseException
import java.text.SimpleDateFormat


class Registro : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registro)
    }

    private fun pickImageFromGallery() {
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent, IMAGE_PICK_CODE)
    }


    companion object {
        private val IMAGE_PICK_CODE = 1000;

        private val PERMISSION_CODE = 1001;
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when(requestCode){
            PERMISSION_CODE -> {
                if (grantResults.size > 0 && grantResults[0] ==
                    PackageManager.PERMISSION_GRANTED){
                    pickImageFromGallery()
                }else{
                    Toast.makeText(this,"permiso denegado", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(resultCode == Activity.RESULT_OK && requestCode == IMAGE_PICK_CODE){
            imgButton.setImageURI(data?.data)
        }
    }

    fun setImg(){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M ){
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED){
                val permissions = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE);

                requestPermissions(permissions, PERMISSION_CODE);
            }else{
                pickImageFromGallery()
            }
        }
        else{
            pickImageFromGallery()
        }
    }

    fun saveData(){
        val name:String=edtName.text.toString()
        val mail:String=edtMail.text.toString()
        val phone:String=edtPhone.text.toString()
        val address:String=edtAddress.text.toString()
        val date:String=edtDate.text.toString()

        if (!name.isEmpty() && !mail.isEmpty() && !phone.isEmpty() && !address.isEmpty() && !date.isEmpty()){
            val pattern = Patterns.EMAIL_ADDRESS
            if(pattern.matcher(mail).matches()) {
                if (phone.length == 10) {
                    val ref = FirebaseDatabase.getInstance().getReference("Users")
                    val userId = ref.push().key
                    val user = Users(userId as String, name, mail, phone, address, date)
                    ref.child(userId).setValue(user).addOnCompleteListener {
                        Toast.makeText(this, "Datos guardados", Toast.LENGTH_SHORT).show()
                        val intent = Intent(this, UsersList::class.java)
                        startActivity(intent)
                    }


                } else {
                    Toast.makeText(this, "El telefono debe tener 10 digitos", Toast.LENGTH_SHORT).show()
                }
            }else{
                Toast.makeText(this, "Error en la estructura del correo", Toast.LENGTH_SHORT).show()
            }
        } else{
            Toast.makeText(this,"Hay campos sin llenar", Toast.LENGTH_SHORT).show()
        }

    }

}
